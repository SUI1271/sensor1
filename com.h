#ifndef COM_H
#define COM_H

#include <QObject>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QDebug>

class Com : public QObject
{
    Q_OBJECT
public:
    explicit Com(QObject *parent = nullptr);
    void ComPortAvailable();

signals:
    void GetComPortList(QVariant text);
    void ReadMessage(QVariant text);

public slots:
    void SetComPortParams(const QString& Com, const QString& BaudRate, const QString& Parity);
    void SendMessage(const QString& Message);
    void MessageAvailable();

};

#endif // COM_H
