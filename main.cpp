#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickWindow>
#include <QQmlContext>
#include <QtSerialPort/QtSerialPort>
#include <com.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    Com com;


    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    QObject *topLevel = engine.rootObjects().value(0);
    QQuickWindow *window = qobject_cast<QQuickWindow *>(topLevel);

    // connect QML signal to C++ slot
    QObject::connect(window, SIGNAL(setComPortParams(QString, QString, QString)),
                     &com, SLOT(SetComPortParams(QString, QString, QString)));

    // connect QML signal to C++ slot
    QObject::connect(window, SIGNAL(sendMessage(QString)),
                     &com, SLOT(SendMessage(QString)));

    // connect our C++ signal to our QML slot
    // NOTE: To pass a parameter to QML slot, it has to be a QVariant.
    QObject::connect(&com, SIGNAL(GetComPortList(QVariant)),
                     window, SLOT(getComPortList(QVariant)));

    // connect our C++ signal to our QML slot
    QObject::connect(&com, SIGNAL(ReadMessage(QVariant)),
                     window, SLOT(readMessage(QVariant)));

    com.ComPortAvailable();


    return app.exec();
}
