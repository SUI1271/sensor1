#include "com.h"
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QDebug>
#include <QTimer>

QSerialPort Serial;

Com::Com(QObject *parent) : QObject(parent)
{

}


void Com::ComPortAvailable()
{
    // Checks which ports (Range 1-20) are available.
    // and emits a list with available com ports

//    QSerialPort Serial;

    QStringList ComPortsAvailable;
    QString sPort;
/*
    bool bOK;
    int iPort = 1;

    while (iPort <= 20)
        {
            sPort = "COM"+QString::number(iPort);
            Serial.setPortName(sPort);
            bOK = Serial.open(QSerialPort::ReadWrite);
            if (bOK)
            {
                Serial.close();
                ComPortsAvailable.append(sPort);
            }
            iPort++;
    }
    emit GetComPortList(ComPortsAvailable);
    qDebug() << "com.cpp [1] ComPortsAvailable >: " << ComPortsAvailable;

    QSerialPortInfo Info;
    int iTemp;
    iTemp = Info.availablePorts().count();
 */

    qDebug() << "Number of serial ports ->: " << QSerialPortInfo::availablePorts().count();
    foreach (const QSerialPortInfo port, QSerialPortInfo::availablePorts())
    {
        qDebug() << "Port Name ->: " << port.portName();
        ComPortsAvailable.append(port.portName());
    }
    emit GetComPortList(ComPortsAvailable);
}


void Com::SetComPortParams(const QString& Com, const QString& BaudRate, const QString& Parity)
{
    bool bOK;

    Serial.close();
    Serial.setPortName(Com);
    Serial.setBaudRate(BaudRate.toInt());
    Serial.setDataBits(QSerialPort::Data8);
    Serial.setParity(QSerialPort::NoParity);
    bOK = Serial.open(QSerialPort::ReadWrite);
    if (bOK)
    {
        Serial.write("Port succesfully opened!");
        Serial.write("\r\n");
        Serial.flush();

        QTimer *timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()), this, SLOT(MessageAvailable()));
        timer->start(50);
    }
    qDebug() << "com.cpp [2] SetComPortParams: ComPort: " << Com << " BaudRate: " << BaudRate << " Parity: " << Parity;
}

void Com::SendMessage(const QString& Message)
{
    QByteArray s;
    s.append(Message);
    qDebug() << "s";
    Serial.write(s);
    Serial.flush();
}

void Com::MessageAvailable()
{
    QByteArray sRead;
    QByteArray sComplete;
    static QByteArray sRest;
    int nLengthRead;
    char cMsgTerminator = '\n';

    sRead = Serial.readAll();
    nLengthRead = sRead.length();

    if (nLengthRead > 0)
    {
        if (sRead.contains(cMsgTerminator))
        {
            sComplete = sRest;
            sComplete.append(sRead.left((sRead.indexOf(cMsgTerminator))+1));
            sRest = sRead.right(nLengthRead-((sRead.indexOf(cMsgTerminator))+1));
        }
        else
        {
            sRest.append(sRead);
            sComplete.append(sRead);
        }

        if (sRead.contains(cMsgTerminator))
        {
            emit ReadMessage(sComplete);
                 qDebug() << sComplete;
                 sComplete = NULL;
        }
    }
//    qDebug() << "LengthSRest: " << sRest.length() << "LengthSComplete: " << sComplete.length() << "nLengthRead: " << nLengthRead ;

}
