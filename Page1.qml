import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3

Page1Form {
    property int counter : 0
    property string sendString :""
    property string displayString: ""

    scrollView.onHeightChanged: {
    textAreaSend.cursorPosition = 0
    textAreaSend.cursorPosition = textAreaSend.length
}
    scrollView1.onHeightChanged: {
    textAreaReceive.cursorPosition = 0
    textAreaReceive.cursorPosition = textAreaReceive.length
}

    buttonClearSend.onClicked: {
        textAreaSend.clear()
}

    buttonClearReceive.onClicked: {
        textAreaReceive.clear()
}

    buttonSend.onClicked: {

//      console.log("Page1.qml [1] Entered text: " + textField1.text);
        sendString = textField1.text;
        displayString = textField1.text;
        if (checkBoxCR.checked) {
            sendString = sendString +"\r"
            displayString = displayString + "<CR>"
        }
        if (checkBoxLF.checked) {
            sendString = sendString +"\n"
            displayString = displayString + "<LF>"
        }

        sendMessage(sendString)

        counter = counter + 1
        textAreaSend.append(counter + " -> " + displayString)
        textAreaSend.cursorPosition = textAreaSend.length
        displayString = "";
        sendString = "";
    }
}
