import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Page2Form {

    comboBoxComPort.onCurrentTextChanged: {
        setComPortParams(comboBoxComPort.currentText,comboBoxBaudRate.currentText,comboBoxParity.currentText)
}

    comboBoxBaudRate.onCurrentTextChanged: {
        setComPortParams(comboBoxComPort.currentText,comboBoxBaudRate.currentText,comboBoxParity.currentText)
}

    comboBoxParity.onCurrentTextChanged: {
         setComPortParams(comboBoxComPort.currentText,comboBoxBaudRate.currentText,comboBoxParity.currentText)
}

    checkBoxTransparency.onCheckedChanged: {
        if (checkBoxTransparency.checked == true)
           root.textAreaBackground = "transparent"
        else root.textAreaBackground = root.textAreaBackgroundDefault
        if (checkBoxTransparency.checked == true)
           root.textAreaText = "black"
        else root.textAreaText = root.textAreaTextDefault
}

    comboBoxBaudRate.model:["1200","2400","4800","9600","19200","38400","57600","115200"]
    comboBoxComPort.model:["COM1","COM2"]
    comboBoxParity.model:["None","Even","Odd"]

}
