import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3

Item
{
    property alias textField1: textField1
    property alias checkBoxCR: checkBoxCR
    property alias checkBoxLF: checkBoxLF
    property alias textAreaSend: textAreaSend
    property alias textAreaReceive: textAreaReceive
    property alias buttonSend: buttonSend
    property alias buttonClearSend: buttonClearSend
    property alias buttonClearReceive: buttonClearReceive
    property alias scrollView: scrollView
    property alias scrollView1: scrollView1

    TextField {
        id: textField1
        anchors.left: parent.left
        anchors.leftMargin: 40
        y: 40
        anchors.right: buttonSend.left
        anchors.rightMargin: 20
        height: 30
        text: qsTr("Text Field")
        font.pointSize: 12
        color: root.textAreaText
        background: Rectangle { color: root.textAreaBackground
        border.color: "white"
        border.width: 1 }
}

    Button {
        id: buttonSend
        anchors.right: parent.right
        anchors.rightMargin: 40
        y: textField1.y
        width: 80
        height: 30
        text: qsTr("Send")
        spacing: 0
        font.pointSize: 12
        font.capitalization: Font.MixedCase
    }

   ScrollView {
        id: scrollView
        anchors.left: parent.left
        anchors.leftMargin: 40
        anchors.top: textField1.bottom
        anchors.topMargin: 30
        anchors.right: buttonSend.left
        anchors.rightMargin: 20
//        height: 140
        height: (parent.height -180)/2
        ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
        ScrollBar.vertical.policy: ScrollBar.AlwaysOn

        TextArea {
            id: textAreaSend
            text: "..."
            font.pointSize: 12
            wrapMode: Text.WordWrap
            readOnly: true
            color: root.textAreaText
            background: Rectangle {
                border.color: "white"
                border.width: 1
                color: root.textAreaBackground
 /*               gradient: Gradient {
                    GradientStop {
                        position: 0
                        color: "#012582"
                    }
                    GradientStop {
                        position: 1
                        color: "#000000"
                    }
                }*/
            }
        }
   }

   CheckBox {
       id: checkBoxCR
       anchors.left: buttonSend.left
       anchors.top: scrollView.top
       width: 75
       height: 35
       font.pointSize: 12
       text: "<font color=\"white\">CR</font>"
       topPadding: 0
       leftPadding: 0
       spacing: 10
   }

    CheckBox {
        id: checkBoxLF
        anchors.left: buttonSend.left
        anchors.top: checkBoxCR.bottom
        anchors.topMargin: 10
        width: 75
        height: 35
        font.pointSize: 12
        text: "<font color=\"white\">LF</font>"
        leftPadding: 0
        spacing: 10
    }

    Button {
        id: buttonClearSend
        anchors.right: parent.right
        anchors.rightMargin: 40
        anchors.bottom: scrollView.bottom
        width: 80
        height: 30
        text: qsTr("Clear")
        font.pointSize: 12
        checked: false
        checkable: false
    }

    ScrollView {
        id: scrollView1
        anchors.left: parent.left
        anchors.leftMargin: 40
        anchors.right: buttonSend.left
        anchors.rightMargin: 20
        anchors.top: scrollView.bottom
        anchors.topMargin: 20
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 50

        ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
        ScrollBar.vertical.policy: ScrollBar.AlwaysOn

        TextArea {
            id: textAreaReceive
            text: "..."
            font.pointSize: 12
            wrapMode: Text.WordWrap
            readOnly: true
            color: root.textAreaText
            background: Rectangle {
                border.color: "white"
                border.width: 1
                color: root.textAreaBackground
/*
                gradient: Gradient {
                    GradientStop {
                        position: 0
                        color: "#012582"
                    }

                    GradientStop {
                        position: 1
                        color: "#000000"
                    }
                }
*/
            }
        }
    }

    Button {
        id: buttonClearReceive
        anchors.right: parent.right
        anchors.rightMargin: 40
        anchors.bottom: scrollView1.bottom
        width: 80
        height: 30
        text: qsTr("Clear")
        font.pointSize: 12
    }

    Image {
        id: image
        fillMode: Image.PreserveAspectCrop
        opacity: 0.7
        z: -1
        anchors.fill: parent
        source: "Wasser.png"
    }

/*
    Rectangle {
        id: rectangle
        z: -2
        anchors.fill: parent
        color: "#02164b"

        gradient: Gradient {
            GradientStop {
                position: 0.827
                color: "#262449"
            }

            GradientStop {
                position: 0
                color: "#2063cc"
            }

        }
    }*/
}
