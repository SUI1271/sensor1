import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

ApplicationWindow {

    property var comPortList
    property var textAreaBackground
    property var textAreaBackgroundDefault
    property var textAreaText
    property var textAreaTextDefault
    property string messageToCheck :""
    property string messageReceived :""
    property int numberOk : 0
    property int numberFailed : 0
    property bool  boCheck : false

    // this signal is sent to C++ slot (com.cpp)
    signal setComPortParams(string Com, string BaudRate, string Parity)

    // this signal is sent to C++ slot (com.cpp)
    signal sendMessage(string Message)

    // this slot receives signal from C++ (com.cpp)
    function getComPortList(text)
    {
        console.log("main.qml [1] setComPortList: " + text)
        comPortList = text
    }

    // this slot receives signal from C++ (com.cpp)
    function readMessage(text)
    {
//      console.log("main.qml [2] readMessage--: " + text)
        page1.updateTextArea(text)
        page3.updateTextArea(text)
    }

    id: root
    visible: true
    width: 640
    height: 480
    title: qsTr("Sensor")
    minimumHeight: 440
    minimumWidth: 330
    textAreaBackgroundDefault: "black"
    textAreaBackground : textAreaBackgroundDefault
    textAreaTextDefault: "white"
    textAreaText : textAreaTextDefault

    SwipeView {
        id: swipeView
        anchors.fill: parent
//        currentIndex: tabBar.currentIndex

        Page1 {
            id: page1

            function updateTextArea (text)
            {
                textAreaReceive.insert(textAreaReceive.length,numberOk)
                textAreaReceive.insert(textAreaReceive.length,"/")
                textAreaReceive.insert(textAreaReceive.length,numberFailed)
                textAreaReceive.insert(textAreaReceive.length," - ")
                textAreaReceive.insert(textAreaReceive.length,text)
                if (textAreaReceive.length > 3000) textAreaReceive.remove(0,100)
            }
        }

        Page3 {
            id: page3

            function updateTextArea (text)
            {
                if (textAreaReceive.length > 3000) textAreaReceive.remove(0,100);
                messageReceived = text

                if (boCheck == true)
                {
                    if (messageToCheck != messageReceived)
                    {
                        numberFailed = numberFailed + 1
                        textAreaReceive.insert(textAreaReceive.length,text);
                    }
                    else
                    {
                        numberOk = numberOk + 1
                    }
                    labelMessageOK.text = numberOk
                    labelMessageFailed.text = numberFailed
                }
            }
        }

        Page2 {
            id: page2
            comboBoxComPort.model:comPortList
        }


    }
/*
    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        TabButton {
            text: qsTr("Terminal")
        }
        TabButton {
            text: qsTr("Configuration")
        }
    } */

}
