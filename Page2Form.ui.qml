import QtQuick 2.7
import QtQuick.Controls 2.2

Item {
    width: 640
    height: 480
    property alias checkBoxTransparency: checkBoxTransparency
    property alias comboBoxParity: comboBoxParity
    property alias comboBoxComPortTextRole: comboBoxComPort.textRole
    property alias comboBoxComPort: comboBoxComPort
    property alias comboBoxBaudRate: comboBoxBaudRate

    ComboBox {
        id: comboBoxComPort
        x: parent.width/2 -50
        y: 38
        width: 150
        font.family: "Tahoma"
        font.pointSize: 12
        background: Rectangle {
            border.color: "white"
            border.width: 2
            color: "transparent"
        }

        delegate: ItemDelegate {
            width: comboBoxComPort.width
            contentItem: Text {
                text: modelData
                font.pointSize: comboBoxComPort.font.pointSize
                font.family: comboBoxComPort.font.family
                elide: Text.ElideRight
                verticalAlignment: Text.AlignVCenter
            }
            highlighted: comboBoxComPort.highlightedIndex === index
        }
    }

    ComboBox {
        id: comboBoxBaudRate
        x: parent.width/2 -50
        y: 88
        width: 150
        font.family: "Tahoma"
        font.pointSize: 12
        currentIndex: 6
        background: Rectangle {
            border.color: "white"
            border.width: 2
            color: "transparent"
        }

        delegate: ItemDelegate {
            width: comboBoxBaudRate.width
            contentItem: Text {
                text: modelData
                font.pointSize: comboBoxBaudRate.font.pointSize
                font.family: comboBoxBaudRate.font.family
                elide: Text.ElideRight
                verticalAlignment: Text.AlignVCenter
            }
            highlighted: comboBoxBaudRate.highlightedIndex === index
        }
    }

    ComboBox {
        id: comboBoxParity
        x: parent.width/2 -50
        y: 138
        width: 150
        font.family: "Tahoma"
        font.pointSize: 12
        background: Rectangle {
            border.color: "white"
            border.width: 2
            color: "transparent"
        }

        delegate: ItemDelegate {
            width: comboBoxParity.width
            contentItem: Text {
                text: modelData
                font.pointSize: comboBoxParity.font.pointSize
                font.family: comboBoxParity.font.family
                elide: Text.ElideRight
                verticalAlignment: Text.AlignVCenter
            }
            highlighted: comboBoxParity.highlightedIndex === index
        }
    }


    Image {
        id: image
        fillMode: Image.PreserveAspectCrop
        opacity: 0.7
        z: -1
        anchors.fill: parent
        source: "Wasser.png"
    }

    Label {
        id: label
        x: 223
        y: 43
        text: qsTr("Com Port :")
        font.pointSize: 12
        font.family: "Tahoma"
        anchors.right: comboBoxComPort.left
        anchors.rightMargin: 20
    }

    Label {
        id: label1
        x: 218
        y: 93
        text: qsTr("Baud Rate :")
        font.pointSize: 12
        font.family: "Tahoma"
        anchors.right: comboBoxBaudRate.left
        anchors.rightMargin: 20
    }

    Label {
        id: label2
        x: 249
        y: 143
        text: qsTr("Parity :")
        font.pointSize: 12
        anchors.right: comboBoxParity.left
        anchors.rightMargin: 20
    }

    CheckBox {
        id: checkBoxTransparency
        x: parent.width/2 -50
        y: 255
        text: qsTr("Transparency")
        font.pointSize: 11
    }
}
